<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_types', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name');
			$table->integer('createdBy')->nullable;	//references users table
            $table->timestamps();			
        	$table->softDeletes();
        });
		
		DB::table('company_types')->insert(array(
			'name' => 'Standard'
		));
		DB::table('company_types')->insert(array(
			'name' => 'Black Listed'
		));
		DB::table('company_types')->insert(array(
			'name' => "Preferred"
		));
		DB::table('company_types')->insert(array(
			'name' => "Conditional"
		));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_types');
    }
}
