<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTruckMakeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('truck_makes', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name');
			$table->integer('createdBy')->nullable();	//references users table
            $table->timestamps();
        	$table->softDeletes();
        });
		DB::table('truck_makes')->insert(array(
			'name' => 'N/A'
		));
		DB::table('truck_makes')->insert(array(
			'name' => 'Freightliner'
		));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('truck_makes');
    }
}
