<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscrepanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discrepancies', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('payment');					//references payments table
			$table->float('discrepancy');
			$table->string('comment')->nullable();
			$table->boolean('isResolved')->default(0);
			$table->integer('createdBy')->nullable;	//references users table
            $table->timestamps();			
        	$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('discrepancies');
    }
}
