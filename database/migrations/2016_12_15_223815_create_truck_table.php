<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTruckTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trucks', function (Blueprint $table) {
            $table->increments('id');
			$table->string('number');
			$table->integer('driver'); 				//references drivers table
			$table->integer('year');				//references years table
			$table->string('vin')->nullable();		
			$table->integer('make')->nullable();	//references truck_makes table
			$table->integer('model')->nullable();	//references truck_models table
			$table->integer('createdBy')->nullable();	//references users table
			$table->boolean('isActive')->default(1); 
            $table->timestamps();
        	$table->softDeletes();
        });
		
		DB::table('trucks')->insert(array(
			'number' => 0000,
			'driver' => 1,
			'year' => 1,
			'make' => 1,
			'model' => 1
		));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trucks');
    }
}
