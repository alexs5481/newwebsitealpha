<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->isnullable();
            $table->string('email')->unique();
            $table->string('password');
			$table->integer('userType')->default(1); 		//references userTypes table
			$table->boolean('isActive')->default(1); 
            $table->rememberToken();
            $table->timestamps();
        });
		
		DB::table('users')->insert(array(
			'email' => 'admin@alphaproboard.com',
			//'username' => 'super_admin',
			'password' => bcrypt('adminpassword'),
			'userType' => 2,
			'isActive' => 1
		));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
