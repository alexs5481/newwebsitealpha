<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loads', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('proNumber')->unique();
			$table->integer('company');   				//references companies table
			$table->integer('truck')->nullable(); 		//references trucks table
			$table->integer('loadAmount');				//references amounts table
			$table->integer('driverPay')->nullable();	//references amounts table
			$table->integer('broker');					//references brokers table
			$table->integer('invoice')->nullable();		//references invoices table
			$table->integer('paymentType')->nullable();	//references payment_types table
			$table->integer('loadType')->nullable();	//references load_types table
			$table->string('comments')->nullable();
			$table->integer('createdBy')->nullable();	//references users table
            $table->timestamps();			
        	$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('loads');
    }
}
