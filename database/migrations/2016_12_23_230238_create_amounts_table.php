<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amounts', function (Blueprint $table) {
            $table->increments('id');
			$table->float('amount')->unique();
			$table->integer('load');   					//references loads table
			$table->integer('type')->nullable(); 		//references amount_types table
			$table->integer('createdBy')->nullable(); 	//references users table
            $table->timestamps();			
        	$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('amounts');
    }
}
