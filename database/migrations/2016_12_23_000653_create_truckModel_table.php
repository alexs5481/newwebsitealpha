<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTruckModelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('truck_models', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('make'); 		//references truck_makes table
			$table->string('name');
			$table->integer('createdBy')->nullable();	//references users table
            $table->timestamps();
        	$table->softDeletes();
        });
		DB::table('truck_models')->insert(array(
			'name' => 'N/A',
			'make' => 1
		));
		DB::table('truck_models')->insert(array(
			'name' => 'Cascadia',
			'make' => 2
		));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('truck_models');
    }
}
