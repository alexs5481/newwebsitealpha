<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyRatingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_ratings', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('rating'); 			//references ratingTypes table
			$table->date('date');
			$table->integer('company');				//references companies table
			$table->integer('createdBy')->nullable;	//references users table
            $table->timestamps();			
        	$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_ratings');
    }
}
