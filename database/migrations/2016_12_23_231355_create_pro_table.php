<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pros', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('number');
			$table->integer('createdBy')->nullable(); 	//references users table
			$table->boolean('available');
            $table->timestamps();			
        	$table->softDeletes();
        });
		DB::table('pros')->insert(array(
			'number' => '17000',
			'available' => 1
		));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pros');
    }
}
