<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoadTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('load_types', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name');
			$table->integer('createdBy')->nullable;	//references users table
            $table->timestamps();			
        	$table->softDeletes();
        });
		DB::table('load_types')->insert(array(
			'name' => 'Full'
		));
		DB::table('load_types')->insert(array(
			'name' => 'LTL'
		));
		DB::table('load_types')->insert(array(
			'name' => 'TONU'
		));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('load_types');
    }
}
