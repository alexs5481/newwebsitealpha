<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name');
			$table->string('state')->nullable();
			$table->string('city')->nullable();
			$table->string('mc');
			$table->string('dot');
			$table->boolean('isActive');
			$table->integer('companyType')->nullable();	//references company_types table
			$table->integer('rating')->nullable();	//references company_ratings table
			$table->string('comments')->nullable();	
			$table->integer('createdBy')->nullable;		//references users table
            $table->timestamps();			
        	$table->softDeletes();
			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }
}
