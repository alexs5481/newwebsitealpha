<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_types', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name');
			$table->integer('createdBy')->nullable;	//references users table
            $table->timestamps();
        	$table->softDeletes();
        });
		
		DB::table('driver_types')->insert(array(
			'name' => 'Owner Op' 					//owner operator
		));
		DB::table('driver_types')->insert(array(
			'name' => 'Company Driver' 
		));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('driver_types');
    }
}
