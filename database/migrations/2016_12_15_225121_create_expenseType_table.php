<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpenseTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('expense_types', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name');
			$table->string('comment')->nullable();
			$table->integer('createdBy')->nullable;	//references users table
            $table->timestamps();
        	$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('expense_types');
    }
}
