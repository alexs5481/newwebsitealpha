<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('years', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('year');
			$table->integer('createdBy')->nullable;	//references users table
            $table->timestamps();
        	$table->softDeletes();
        });
		
		DB::table('years')->insert(array(
			'year' => 2020
		));
		DB::table('years')->insert(array(
			'year' => 2019 
		));
		DB::table('years')->insert(array(
			'year' => 2018
		));
		DB::table('years')->insert(array(
			'year' => 2017
		));
		DB::table('years')->insert(array(
			'year' => 2016 
		));
		DB::table('years')->insert(array(
			'year' => 2015
		));
		DB::table('years')->insert(array(
			'year' => 2014 
		));
		DB::table('years')->insert(array(
			'year' => 2013
		));
		DB::table('years')->insert(array(
			'year' => 2012			
		));
		DB::table('years')->insert(array(
			'year' => 2011
		));
		DB::table('years')->insert(array(
			'year' => 2010			
		));
		DB::table('years')->insert(array(
			'year' => 2009
		));
		DB::table('years')->insert(array(
			'year' => 2008 
		));
		DB::table('years')->insert(array(
			'year' => 2007
		));
		DB::table('years')->insert(array(
			'year' => 2006 
		));
		DB::table('years')->insert(array(
			'year' => 2005
		));
		DB::table('years')->insert(array(
			'year' => 2004			
		));
		DB::table('years')->insert(array(
			'year' => 2003
		));
		DB::table('years')->insert(array(
			'year' => 2002			
		));
		DB::table('years')->insert(array(
			'year' => 2001
		));
		DB::table('years')->insert(array(
			'year' => 2000 
		));
		DB::table('years')->insert(array(
			'year' => 1999
		));
		DB::table('years')->insert(array(
			'year' => 1998 
		));
		DB::table('years')->insert(array(
			'year' => 1997
		));
		DB::table('years')->insert(array(
			'year' => 1996			
		));
		DB::table('years')->insert(array(
			'year' => 1995
		));
		DB::table('years')->insert(array(
			'year' => 1994			
		));
		DB::table('years')->insert(array(
			'year' => 1993
		));
		DB::table('years')->insert(array(
			'year' => 1992 
		));
		DB::table('years')->insert(array(
			'year' => 1991
		));
		DB::table('years')->insert(array(
			'year' => 1990 
		));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('years');
    }
}
