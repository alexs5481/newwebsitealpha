<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLumperTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lumpers', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('load');
			$table->float('amount');
			$table->boolean('isPaid')->default(0); 
			$table->integer('createdBy')->nullable;	//references users table
            $table->timestamps();			
        	$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lumpers');
    }
}
