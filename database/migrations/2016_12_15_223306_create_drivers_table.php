<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('type');					//references driverTypes table
			$table->string('fname');
			$table->string('lname')->nullable();
			$table->date('DOB')->nullable(); 			//date of birth
			$table->string('phoneNumber')->nullable(); 
			$table->boolean('isActive')->default(0); 
			$table->integer('createdBy')->nullable;		//references users table
            $table->timestamps();			
        	$table->softDeletes();
        });
		
		DB::table('drivers')->insert(array(
			'type' => 2,
			'fname' => 'N/A'
		));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('drivers');
    }
}
