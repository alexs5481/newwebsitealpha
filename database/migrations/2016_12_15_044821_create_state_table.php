<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name');
			$table->string('stateCode');
			$table->integer('createdBy')->nullable;	//references users table
            $table->timestamps();			
        	$table->softDeletes();
			
        });
		
		DB::table('states')->insert(array(
			'name' => 'Alaska',
			'stateCode' => 'AK'
		));
		DB::table('states')->insert(array(
			'name' => 'Alabama',
			'stateCode' => 'AL'
		));
		DB::table('states')->insert(array(
			'name' => 'Arkansas',
			'stateCode' => 'AR'
		));
		DB::table('states')->insert(array(
			'name' => 'Arizona',
			'stateCode' => 'AZ'
		));
		DB::table('states')->insert(array(
			'name' => 'California',
			'stateCode' => 'CA'
		));
		DB::table('states')->insert(array(
			'name' => 'Colorado',
			'stateCode' => 'CO'
		));
		DB::table('states')->insert(array(
			'name' => 'Connecticut',
			'stateCode' => 'CT'
		));
		DB::table('states')->insert(array(
			'name' => 'District of Columbia',
			'stateCode' => 'DC'
		));
		DB::table('states')->insert(array(
			'name' => 'Delaware',
			'stateCode' => 'DE'
		));
		DB::table('states')->insert(array(
			'name' => 'Florida',
			'stateCode' => 'FL'
		));
		DB::table('states')->insert(array(
			'name' => 'Georgia',
			'stateCode' => 'GA'
		));
		DB::table('states')->insert(array(
			'name' => 'Hawaii',
			'stateCode' => 'HI'
		));
		DB::table('states')->insert(array(
			'name' => 'Iowa',
			'stateCode' => 'IA'
		));
		DB::table('states')->insert(array(
			'name' => 'Idaho',
			'stateCode' => 'ID'
		));
		DB::table('states')->insert(array(
			'name' => 'Illinois',
			'stateCode' => 'IL'
		));
		DB::table('states')->insert(array(
			'name' => 'Indiana',
			'stateCode' => 'IN'
		));
		DB::table('states')->insert(array(
			'name' => 'Kansas',
			'stateCode' => 'KS'
		));
		DB::table('states')->insert(array(
			'name' => 'Kentucky',
			'stateCode' => 'KY'
		));
		DB::table('states')->insert(array(
			'name' => 'Louisiana',
			'stateCode' => 'LA'
		));
		DB::table('states')->insert(array(
			'name' => 'Massachusetts',
			'stateCode' => 'MA'
		));
		DB::table('states')->insert(array(
			'name' => 'Maryland',
			'stateCode' => 'MD'
		));
		DB::table('states')->insert(array(
			'name' => 'Maine',
			'stateCode' => 'ME'
		));
		DB::table('states')->insert(array(
			'name' => 'Michigan',
			'stateCode' => 'MI'
		));
		DB::table('states')->insert(array(
			'name' => 'Minnesota',
			'stateCode' => 'MN'
		));
		DB::table('states')->insert(array(
			'name' => 'Missouri',
			'stateCode' => 'MO'
		));
		DB::table('states')->insert(array(
			'name' => 'Mississippi',
			'stateCode' => 'MS'
		));
		DB::table('states')->insert(array(
			'name' => 'Montana',
			'stateCode' => 'MT'
		));
		DB::table('states')->insert(array(
			'name' => 'New Brunswick',
			'stateCode' => 'NB'
		));
		DB::table('states')->insert(array(
			'name' => 'North Carolina',
			'stateCode' => 'NC'
		));
		DB::table('states')->insert(array(
			'name' => 'North Dakota',
			'stateCode' => 'ND'
		));
		DB::table('states')->insert(array(
			'name' => 'Nebraska',
			'stateCode' => 'NE'
		));
		DB::table('states')->insert(array(
			'name' => 'New Hampshire',
			'stateCode' => 'NH'
		));
		DB::table('states')->insert(array(
			'name' => 'New Jersey',
			'stateCode' => 'NJ'
		));
		DB::table('states')->insert(array(
			'name' => 'New Mexico',
			'stateCode' => 'NM'
		));
		DB::table('states')->insert(array(
			'name' => 'Nova Scotia',
			'stateCode' => 'NS'
		));
		DB::table('states')->insert(array(
			'name' => 'Nevada',
			'stateCode' => 'NV'
		));
		DB::table('states')->insert(array(
			'name' => 'New York',
			'stateCode' => 'NY'
		));
		DB::table('states')->insert(array(
			'name' => 'Ohio',
			'stateCode' => 'OH'
		));
		DB::table('states')->insert(array(
			'name' => 'Oklahoma',
			'stateCode' => 'OK'
		));
		DB::table('states')->insert(array(
			'name' => 'Ontario',
			'stateCode' => 'ON'
		));
		DB::table('states')->insert(array(
			'name' => 'Oregon',
			'stateCode' => 'OR'
		));
		DB::table('states')->insert(array(
			'name' => 'Pennsylvania',
			'stateCode' => 'PA'
		));
		DB::table('states')->insert(array(
			'name' => 'Prince Edward Island',
			'stateCode' => 'PE'
		));
		DB::table('states')->insert(array(
			'name' => 'Quebec',
			'stateCode' => 'QC'
		));
		DB::table('states')->insert(array(
			'name' => 'Rhode Island',
			'stateCode' => 'RI'
		));
		DB::table('states')->insert(array(
			'name' => 'South Carolina',
			'stateCode' => 'SC'
		));
		DB::table('states')->insert(array(
			'name' => 'South Dakota',
			'stateCode' => 'SD'
		));
		DB::table('states')->insert(array(
			'name' => 'Tennessee',
			'stateCode' => 'TN'
		));
		DB::table('states')->insert(array(
			'name' => 'Texas',
			'stateCode' => 'TX'
		));
		DB::table('states')->insert(array(
			'name' => 'Utah',
			'stateCode' => 'UT'
		));
		DB::table('states')->insert(array(
			'name' => 'Virginia',
			'stateCode' => 'VA'
		));
		DB::table('states')->insert(array(
			'name' => 'Vermont',
			'stateCode' => 'VT'
		));
		DB::table('states')->insert(array(
			'name' => 'Washington',
			'stateCode' => 'WA'
		));
		DB::table('states')->insert(array(
			'name' => 'Wisconsin',
			'stateCode' => 'WI'
		));
		DB::table('states')->insert(array(
			'name' => 'West Virginia',
			'stateCode' => 'WV'
		));
		DB::table('states')->insert(array(
			'name' => 'Wyoming',
			'stateCode' => 'WY'
		));
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('states');
    }
}
