<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rating_types', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name');
			$table->integer('createdBy')->nullable;	//references users table
            $table->timestamps();			
        	$table->softDeletes();
        });
		DB::table('rating_types')->insert(array(
			'name' => 'A'
		));
		DB::table('rating_types')->insert(array(
			'name' => "B"
		));
		DB::table('rating_types')->insert(array(
			'name' => "C"
		));
		DB::table('rating_types')->insert(array(
			'name' => "D"
		));
		DB::table('rating_types')->insert(array(
			'name' => "N"
		));
		DB::table('rating_types')->insert(array(
			'name' => "I"
		));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rating_types');
    }
}
