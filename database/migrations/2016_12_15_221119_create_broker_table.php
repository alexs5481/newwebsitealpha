<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrokerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brokers', function (Blueprint $table) {
            $table->increments('id');
			$table->string('fname');
			$table->string('lname')->nullable();
			$table->string('companyId');
			$table->string('phoneNumber');
			$table->string('email');
			$table->integer('createdBy')->nullable;	//references users table
            $table->timestamps();
        	$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('brokers');
    }
}
