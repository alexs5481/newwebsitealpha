<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeeTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fee_types', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name');
			$table->integer('createdBy')->nullable;	//references users table
            $table->timestamps();			
        	$table->softDeletes();
        });
		DB::table('fee_types')->insert(array(
			'name' => 'Comcheck Fee'
		));
		DB::table('fee_types')->insert(array(
			'name' => 'Advance Fee'
		));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fee_types');
    }
}
