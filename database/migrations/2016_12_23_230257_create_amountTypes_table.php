<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmountTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amount_types', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name');
			$table->integer('createdBy')->nullable(); 	//references users table
            $table->timestamps();			
        	$table->softDeletes();
        });
		
		DB::table('amount_types')->insert(array(
			'name' => 'companyAmount' 					//amount Paid to company	
		));
		DB::table('amount_types')->insert(array(
			'name' => 'driverAmount' 					//amount Paid to driver	
		));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('amount_types');
    }
}
