$(document).ready(function () {
	$('.showable').click (function () {
		var thing = $(this).data('object');
		document.location.href = '/' + thing + '/' + $(this).data('id') + '/edit';
	});
	
	$('.admin-delete').click(function () {
		$(this).closest('form').submit();
	});
	
	$("#companyName").autocomplete({
	      		source: 'searchCompanies.php'
	 });
	 
	 var data = [{ id: 0, text: 'enhancement' }, { id: 1, text: 'bug' }, { id: 2, text: 'duplicate' }, { id: 3, text: 'invalid' }, { id: 4, text: 'wontfix' }];

	$(".js-example-data-array").select2({
	  data: data
	});

	$(".js-example-data-array-selected").select2({
	  data: data
	});
});


