<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Driver extends Model
{
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
	
	public function types () {
		return $this->hasOne('App\DriverType', 'id', 'type');
	}
}
