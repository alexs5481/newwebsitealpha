<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyRating extends Model
{
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
	
	public function companies () {
		return $this->hasOne('App\Company', 'id', 'company');
	}
	public function ratingTypes () {
		return $this->hasOne('App\RatingType', 'id', 'ratingType');
	}
}
