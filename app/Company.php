<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
	
	public function states () {
		return $this->hasOne('App\State', 'id', 'state');
	}
	public function ratings () {
		return $this->hasOne('App\RatingType', 'id', 'rating');
	}
}
