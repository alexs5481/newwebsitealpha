<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Load extends Model
{
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
	
	public function companies () {
		return $this->hasOne('App\Company', 'id', 'company');
	}
	public function trucks () {
		return $this->hasOne('App\Truck', 'id', 'truck');
	}
	public function brokers () {
		return $this->hasOne('App\Broker', 'id', 'broker');
	}
	public function loadTypes () {
		return $this->hasOne('App\LoadType', 'id', 'loadType');
	}
}
