<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Truck extends Model
{
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
	
	public function drivers () {
		return $this->hasOne('App\Driver', 'id', 'driver');
	}
	public function years () {
		return $this->hasOne('App\Year', 'id', 'year');
	}
	public function makes () {
		return $this->hasOne('App\TruckMake', 'id', 'make');
	}
	public function models () {
		return $this->hasOne('App\TruckModel', 'id', 'model');
	}
}
