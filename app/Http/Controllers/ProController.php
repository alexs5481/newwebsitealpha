<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pro;
use App\Http\Requests;

class ProController extends Controller
{
    public function index()
    {
		
    }

    public function create()
    {
		/*$trucks = Truck::all();
		$ratingTypes = RatingType::all();
		$companies = Company::all();
		$brokers = Broker::all();
		$loadTypes = LoadType::all();
		
		$truckArray = array();
		$ratingTypeArray = array();
		$companyArray = array();
		$brokerArray = array();
		$loadTypeArray = array();
		
		foreach($trucks as $truck){
			$truckArray[$truck->id] = $truck->number;
		}
		foreach($ratingTypes as $ratingType){
			$ratingTypeArray[$ratingType->id] = $ratingType->name;
		}
		foreach($companies as $company){
			$companyArray[$company->id] = $company->name;
		}
		foreach($brokers as $broker){
			$brokerArray[$broker->id] = $broker->name;
		}
		foreach($loadTypes as $loadType){
			$loadTypeArray[$loadType->id] = $loadType->name;
		}
		
		$data['truckArray'] = $truckArray;
		$data['ratingTypeArray'] = $ratingTypeArray;
		$data['companyArray'] = $companyArray;
		$data['brokerArray'] = $brokerArray;
		$data['loadTypeArray'] = $loadTypeArray;
		
        return view('loads.load_new', $data);*/
    }

    public static function store(Request $request)
    {   		
		/*$load = new Load();
		$load->proNumber = $request->get('');
		$load->company = $request->get('company');
		$load->truck = $request->get('truckNumber');
		$load->loadAmount = $request->get('amount');
		$load->driverPay = $request->get('driverPay');
		$load->broker = $request->get('broker');
		$load->loadType = $request->get('loadType');
		$load->comments = $request->get('comments');
		$load->save();
		
		return redirect('/loads');*/
		//$lastPro = Pro::all()->orderBy('updated_at', 'desc')->first();
    }
	
	public static function getNextPro()
	{
		$nextAvailable = Pro::where('available', '=', '1')
					->orderBy('number', 'asc')
					->first();
					
		$lastPro = Pro::where('available', '=', '1')
					->orderBy('number', 'desc')
					->first();
					
		if($nextAvailable == $lastPro){
			$number = $nextAvailable->number;
			$nextNumber = $number+1;
			
			$pro = new Pro();
			$pro->number = $nextNumber;
			$pro->available = 1;
			$pro->save();
		}
		return $nextAvailable->number;
	}
	public static function usePro($proNumber){
		
		$pro = Pro::where('number', '=', $proNumber)->update(array(
				'available' => 0
				));
				
		return 1;
	}

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
		/*
		
		
		*/
    }

    public function update(Request $request, $id)
    {
		/*
		$fname = $request->get('fname');
		$lname = $request->get('lname');
		$companyName = $request->get('companyName');
		$companyId = Company::where('name', $companyName)
					->value('id');
		$phoneNumber = $request->get('phoneNumber');
		$email = $request->get('email');
		
		Broker::where('id', $id)->update(array(
			'fname' => $fname,
			'lname' => $lname,
			'companyId' => $companyId,
			'phoneNumber' => $phoneNumber,
			'email' => $email
		));
		
		return redirect('/companies');
		*/
    }

    public function destroy($id)
    {
        //
    }
}
