<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Truck;
use App\Company;
use App\RatingType;
use App\Broker;
use App\Load;
use App\LoadType;
use App\Http\Controllers\ProController;
use Log;
use Illuminate\Support\Facades\Auth;


class LoadController extends Controller
{
    public function index()
    {
						
		$loads = Load::all();
		
		//$data['companies'] = $companies;
		$data['loads'] = $loads;
		
        return view('loads/loads', $data);
    }

    public function create()
    {
		$trucks = Truck::all();
		$ratingTypes = RatingType::all();
		$companies = Company::all();
		$brokers = Broker::all();
		$loadTypes = LoadType::all();
		
		$truckArray = array();
		$ratingTypeArray = array();
		$companyArray = array();
		$brokerArray = array();
		$loadTypeArray = array();
		
		foreach($trucks as $truck){
			$truckArray[$truck->id] = $truck->number;
		}
		foreach($ratingTypes as $ratingType){
			$ratingTypeArray[$ratingType->id] = $ratingType->name;
		}
		foreach($companies as $company){
			$companyArray[$company->id] = $company->name;
		}
		foreach($brokers as $broker){
			$brokerArray[$broker->id] = $broker->name;
		}
		foreach($loadTypes as $loadType){
			$loadTypeArray[$loadType->id] = $loadType->name;
		}
		
		$data['truckArray'] = $truckArray;
		$data['ratingTypeArray'] = $ratingTypeArray;
		$data['companyArray'] = $companyArray;
		$data['brokerArray'] = $brokerArray;
		$data['loadTypeArray'] = $loadTypeArray;
		
        return view('loads.load_new', $data);
    }

    public static function store(Request $request)
    {   		
		
		$nextPro = ProController::getNextPro();
		
		$brokerFullName = $request->get('broker');
		$name = explode(" ", $brokerFullName);
		
		//Log::info($name);
		/*
		if($name[1] != ""){
			$broker = Broker::where('fname', '=', $name[0])
					->where('lname', '=', $name[1])
					->first();
		} else {
			$broker = Broker::where('fname', '=', $name[0])
					->where('lname', '=', "")
					->first();
		}
		Log::info('NextPro: ' . $nextPro . 'Broker Name: ' . $name[0] . ' ' . $name[1] . ' ' . $broker->id );*/
		//Log::info("NextPro: " .$nextPro);
		$driverPay = $request->get('driverPay');
		if(empty($driverPay)){
			$driverPay = $request->get('amount');
		}
		
		$companyName = $request->get('company');
		$company = Company::where('name', "=", $companyName)
					->first();
					
		//Log::info($company->id);
		
		$load = new Load();
		$load->proNumber = $nextPro;
		$load->company = $company->id;
		$load->truck = $request->get('truckNumber');
		$load->loadAmount = $request->get('amount');
		$load->driverPay = $driverPay;
		//$load->broker = $broker->id;
		$load->loadType = $request->get('loadType');
		$load->comments = $request->get('comments');
		$load->save();
		
		ProController::usePro($nextPro);
		
		return redirect('/loads');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
		$trucks = Truck::all();
		$ratingTypes = RatingType::all();
		$companies = Company::all();
		$brokers = Broker::all();
		$loadTypes = LoadType::all();
        $load = Load::find($id);
		
		$truckArray = array();
		$ratingTypeArray = array();
		$companyArray = array();
		$brokerArray = array();
		$loadTypeArray = array();
		
		foreach($trucks as $truck){
			$truckArray[$truck->id] = $truck->number;
		}
		foreach($ratingTypes as $ratingType){
			$ratingTypeArray[$ratingType->id] = $ratingType->name;
		}
		foreach($companies as $company){
			$companyArray[$company->id] = $company->name;
		}
		foreach($brokers as $broker){
			$brokerArray[$broker->id] = $broker->name;
		}
		foreach($loadTypes as $loadType){
			$loadTypeArray[$loadType->id] = $loadType->name;
		}
		
		$data['truckArray'] = $truckArray;
		$data['ratingTypeArray'] = $ratingTypeArray;
		$data['companyArray'] = $companyArray;
		$data['brokerArray'] = $brokerArray;
		$data['loadTypeArray'] = $loadTypeArray;
		$data['load'] = $load;
		
		
		return view('loads.load_edit', $data);
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
