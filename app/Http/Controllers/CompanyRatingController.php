<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanyRating;
use App\Http\Requests;

class CompanyRatingController extends Controller
{
	
    public function index()
    {
        $companyRatings = CompanyRating::all();
		
		$data['companyRatings'] = $companyRatings;
		
		return view ('companyRatings/companyRatings', $data);
    }
	/*
    public function create()
    {
		$states = State::all();
		$companyTypes = CompanyType::all();
		
		$stateArray = array();
		$companyTypesArray = array();
		
		foreach($states as $state){
			$stateArray[$state->id] = $state->stateCode;
		}
		foreach($companyTypes as $type){
			$companyTypesArray[$type->id] = $type->name;
		}
  
		$data['stateArray'] = $stateArray;
		$data['companyTypesArray'] = $companyTypesArray;
		
		
        return view('companies.companies_new', $data);
    }
	
	public function autocomplete(Request $request){
		
		$searchTerm = $request->input('term');
		
		$companies = Company::where('name', 'like', '%'. $searchTerm .  '%')
					->get();
		
		$companyArray = array();
		
		foreach($companies as $driver){
			$companyArray[$driver->id] = $driver->name;
		}
		
		$data = $companyArray;
		
		echo json_encode($data);
	}*/

    public static function store(Request $request)
    {   
		
	
		$companyRating = new CompanyRating();
		$companyRating->rating = $request->get('rating');
		$companyRating->date = date("m/d/Y");
		if($request->get('newCompany') !== null)
        	$companyRating->company = 0;
        else
        	$companyRating->company = $request->get('company');
		
		$companyRating->save();
	
		if($request->get('newCompany') !== null)
        	return $companyRating;
        else
        	return redirect ('');
    }
/*
    public function show($id)
    {
        
    }

    public function edit($id)
    {
        $company = Company::find($id);
		$states = State::all();
		$companyTypes = CompanyType::all();
		
		$stateArray = array();
		$companyTypesArray = array();
		
		foreach($states as $state){
			$stateArray[$state->id] = $state->name;
		}
		foreach($companyTypes as $companyType){
			$companyTypesArray[$companyType->id] = $companyType->name;
		}
		
		$data['company'] = $company;
		$data['stateArray'] = $stateArray;
		$data['companyTypesArray'] = $companyTypesArray;
		
		return view('companies.companies_edit', $data);
    }

    public function update(Request $request, $id)
    {
        $state = $request->get('state');
        $city = $request->get('city');
        $name = $request->get('name');
        $mc = $request->get('mc');
        $dot = $request->get('dot');
        $type = $request->get('type');
        $comments = $request->get('comments');

        Company::where('id', $id)->update(array(
            'state' => $state,
            'city' => $city,
            'name' => $name,
            'mc' => $mc,
            'dot' => $dot,
            'companyType' => $type,
            'comments' => $comments
        ));
		
		return redirect('/companies');
    }

    public function destroy($id)
    {
        //
    }*/
}
