<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Driver;
use App\DriverType;
use App\Http\Requests;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$drivers = Driver::all();
		
		$data['drivers'] = $drivers;
		
        return view('drivers/drivers', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$driverTypes = DriverType::all();

		$driverTypesArray = array();
		
		foreach($driverTypes as $type){
			$driverTypesArray[$type->id] = $type->name;
		}
  
		$data['driverTypesArray'] = $driverTypesArray;
	   
		return view('drivers.drivers_new', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function store(Request $request)
    {   
		$driver = new Driver();
		$driver->type = $request->get ('type');
		$driver->fname = $request->get ('fname');
		$driver->lname = $request->get ('lname');
		$driver->dob = $request->get ('DOB');
		$driver->phoneNumber = $request->get ('phoneNumber');
		$driver->isActive = 1;
		$driver->save();
		
		return redirect('/drivers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $driver = Driver::find($id);
		$driverTypes = DriverType::all();
		
		$driverTypesArray = array();
		
		foreach($driverTypes as $type){
			$driverTypesArray[$type->id] = $type->name;
		}
		
		$data['driver'] = $driver;
		$data['driverTypesArray'] = $driverTypesArray;
		return view('drivers.drivers_edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = $request->get('type');
        $fname = $request->get('fname');
        $lname = $request->get('lname');
        $DOB = $request->get('DOB');
        $phoneNumber = $request->get('phoneNumber');

        Driver::where('id', $id)->update(array(
            'type' => $type,
            'fname' => $fname,
            'lname' => $lname,
            'dob' => $DOB,
            'phoneNumber' => $phoneNumber
        ));
		
		return redirect('/drivers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
