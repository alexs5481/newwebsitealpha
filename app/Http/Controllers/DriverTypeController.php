<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DriverType;
use App\Http\Requests;

class DriverTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$driverTypes = DriverType::all();
		
		$data['driverTypes'] = $driverTypes;
        return view('driverTypes/driverTypes', $data);
    }
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		
		
		return view('driverTypes.driverTypes_new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function store(Request $request)
    {   

		if(!$request->get('name') == ''){
			$driverType = new DriverType();
			$driverType->name = $request->get ('name');
			$driverType->save();
		}
		
		return redirect('/driverTypes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		
		$driverType = DriverType::find($id);
		
		$data['driverType'] = $driverType;
        
		return view('driverTypes.driverTypes_edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->get('name');

        DriverType::where('id', $id)->update(array(
            'name' => $name
        ));
		
		return redirect('/driverTypes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
