<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\State;
use App\CompanyType;
use App\RatingType;
use App\CompanyRating;
use App\Http\Requests;
use Log;

class CompanyController extends Controller
{
    public function index()
    {
        $companies = Company::all();
		
		$data['companies'] = $companies;
		
		return view ('companies/companies', $data);
    }

    public function create()
    {
		$states = State::all();
		$companyTypes = CompanyType::all();
		$ratingTypes = RatingType::all();
		
		$stateArray = array();
		$companyTypesArray = array();
		$ratingTypeArray = array();
		
		foreach($states as $state){
			$stateArray[$state->id] = $state->stateCode;
		}
		foreach($companyTypes as $type){
			$companyTypesArray[$type->id] = $type->name;
		}
		foreach($ratingTypes as $ratingType){
			$ratingTypeArray[$ratingType->id] = $ratingType->name;
		}
  
		$data['stateArray'] = $stateArray;
		$data['companyTypesArray'] = $companyTypesArray;
		$data['ratingTypeArray'] = $ratingTypeArray;
		
		
        return view('companies.companies_new', $data);
    }
	
	public function autocomplete(Request $request){
		$searchTerm = $request->input('term');
		
		$companies = Company::where('name', 'like', '%'. $searchTerm .  '%')
					->get();
		
		$companyArray = array();
		
		foreach($companies as $driver){
			$companyArray[$driver->id] = $driver->name;
		}
		
		$data = $companyArray;
		
		echo json_encode($data);
	}

    public static function store(Request $request)
    {   
	
		$companyRating = CompanyRatingController::store ($request);
	
        $company = new Company();
		$company->name = $request->get ('name');
		$company->state = $request->get ('state');
		$company->city = $request->get ('city');
		$company->mc = $request->get ('mc');
		$company->dot = $request->get ('dot');
		$company->companyType = $request->get ('companyType');
		$company->comments = $request->get ('comments');
		$company->rating = $companyRating->id;
		$company->isActive = 1;
		$company->save();
		
		CompanyRating::where('company', 0)->update(array(
			'company' => $company->id
		));

		return redirect('/companies');
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        $company = Company::find($id);
		$states = State::all();
		$companyTypes = CompanyType::all();
		$ratingTypes = RatingType::all();
		
		$stateArray = array();
		$companyTypesArray = array();
		$ratingTypeArray = array();
		
		foreach($states as $state){
			$stateArray[$state->id] = $state->name;
		}
		foreach($companyTypes as $companyType){
			$companyTypesArray[$companyType->id] = $companyType->name;
		}
		foreach($ratingTypes as $ratingType){
			$ratingTypeArray[$ratingType->id] = $ratingType->name;
		}
		
		$data['company'] = $company;
		$data['stateArray'] = $stateArray;
		$data['companyTypesArray'] = $companyTypesArray;
		$data['ratingTypeArray'] = $ratingTypeArray;
		
		return view('companies.companies_edit', $data);
    }

    public function update(Request $request, $id)
    {
        $state = $request->get('state');
        $city = $request->get('city');
        $name = $request->get('name');
        $mc = $request->get('mc');
        $dot = $request->get('dot');
        $type = $request->get('type');
        $comments = $request->get('comments');
        $rating = $request->get('rating');

        Company::where('id', $id)->update(array(
            'state' => $state,
            'city' => $city,
            'name' => $name,
            'mc' => $mc,
            'dot' => $dot,
            'companyType' => $type,
            'comments' => $comments,
			'rating' => $rating
        ));
		
		return redirect('/companies');
    }

    public function destroy($id)
    {
        //
    }
}
