<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Truck;
use App\Driver;
use App\Year;
use App\TruckMake;
use App\TruckModel;
use App\Http\Requests;

class TruckController extends Controller
{
    public function index()
    {
		$trucks = Truck::all();
		
		$data['trucks'] = $trucks;
		
        return view('trucks/trucks', $data);
    }
	
    public function create()
    {
		$drivers = Driver::all();
		$years = Year::all();
		$makes = TruckMake::all();
		$models = TruckModel::all();

		$driverArray = array();
		$yearArray = array();
		$makeArray = array();
		$modelArray = array();
		
		foreach($drivers as $driver){
			$driverArray[$driver->id] = $driver->fname . " " . $driver->lname;
		}
		foreach($years as $year){
			$yearArray[$year->id] = $year->year;
		}
		foreach($makes as $make){
			$makeArray[$make->id] = $make->name;
		}
		foreach($models as $model){
			$modelArray[$model->id] = $model->name;
		}
  
		$data['driverArray'] = $driverArray;
		$data['yearArray'] = $yearArray;
		$data['modelArray'] = $modelArray;
		$data['makeArray'] = $makeArray;
		
		return view('trucks.trucks_new', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function store(Request $request)
    {   
        $truck = new Truck();
		$truck->number = $request->get ('number');
		$truck->driver = $request->get ('driver');
		$truck->year = $request->get ('year');
		$truck->vin = $request->get ('vin');
		$truck->make = $request->get ('make');
		$truck->model = $request->get ('model');
		$truck->isActive = 1;
		$truck->save();
		
		return redirect('/trucks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$truck = Truck::find($id);
		$drivers = Driver::all();
		$years = Year::all();
		$makes = TruckMake::all();
		$models = TruckModel::all();
		
		$driverArray = array();
		$yearArray = array();
		$makeArray = array();
		$modelArray = array();
		
		foreach($drivers as $driver){
			$driverArray[$driver->id] = $driver->fname . " " . $driver->lname;
		}
		foreach($years as $year){
			$yearArray[$year->id] = $year->year;
		}
		foreach($makes as $make){
			$makeArray[$make->id] = $make->name;
		}
		foreach($models as $model){
			$modelArray[$model->id] = $model->name;
		}
  
		$data['driverArray'] = $driverArray;
		$data['yearArray'] = $yearArray;
		$data['modelArray'] = $modelArray;
		$data['makeArray'] = $makeArray;
		$data['truck'] = $truck;
		
        return view('trucks.trucks_edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$truckNumber = $request->get ('number');
		$driver = $request->get ('driver');
		$year = $request->get ('year');
		$vin = $request->get ('vin');
		$make = $request->get ('make');
		$model = $request->get ('model');
		
		Truck::where('id', $id)->update(array(
            'number' => $number,
            'driver' => $driver,
            'year' => $year,
            'vin' => $vin,
            'make' => $make,
            'model' => $model
        ));
		
		return redirect('/trucks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
