<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Broker;
use App\Company;
use App\Http\Requests;
use Log;



class BrokerController extends Controller
{
    public function index()
    {
        /*$companies = Company::all();
		
		$data['companies'] = $companies;
		
		return view ('companies/companies', $data);*/
    }

    public function create($id)
    {
		$data['companyId'] = $id;
		
		return view('/brokers.brokers_new', $data);
    }
	
	public function autocomplete(Request $request){
		
		$searchTerm = $request->input('term');
		$companyName = $request->input('companyName');
		
		$companyId = Company::where('name', $companyName)
					->value('id');
		
		$brokers = Broker::where('fname', 'like', '%' . $searchTerm . '%')
					->where('companyId', '=', $companyId)
					->get();
		
		$brokerArray = array();
		
		foreach($brokers as $broker){
			$brokerArray[$broker->id] = $broker->fname . " " . $broker->lname;
		}
		
		$data = $brokerArray;
		
		echo json_encode($data);
	}

    public static function store(Request $request)
    {   
		$broker = new Broker();
		$broker->fname = $request->get('fname');
		$broker->lname = $request->get('lname');
		$broker->phoneNumber = $request->get('phoneNumber');
		$broker->email = $request->get('email');
		$broker->companyId = $request->get('companyId');
		$broker->save();
		
		return redirect('/companies');
    }

    public function show($id)
    {
		
		$brokers = Broker::where('companyId', $id)
					->get();
		
		$data['brokers'] = $brokers;
		$data['companyId'] = $id;
		
		return view('brokers/brokers_show', $data);
    }

    public function edit($id)
    {		
		$broker = Broker::find($id);
		
		$data['broker'] = $broker;
		
		return view('brokers.brokers_edit', $data);
    }

    public function update(Request $request, $id)
    {	
		$fname = $request->get('fname');
		$lname = $request->get('lname');
		$companyName = $request->get('companyName');
		$companyId = Company::where('name', $companyName)
					->value('id');
		$phoneNumber = $request->get('phoneNumber');
		$email = $request->get('email');
		
		Broker::where('id', $id)->update(array(
			'fname' => $fname,
			'lname' => $lname,
			'companyId' => $companyId,
			'phoneNumber' => $phoneNumber,
			'email' => $email
		));
		
		return redirect('/companies');
    }

    public function destroy($id)
    {
        //
    }
}
