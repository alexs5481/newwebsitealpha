<?php
use App\Company;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware'=>'web'], function () {
	// splash page
	Route::get('/', 'HomeController@index');
	
	// Authentication Routes...
	Route::get('login', 'Auth\AuthController@showLoginForm');
	Route::post('login', 'Auth\AuthController@authenticate');
	Route::get('logout', 'Auth\AuthController@logout');

	// Registration Routes...
	Route::get('register', 'Auth\AuthController@showRegistrationForm');
	Route::post('register', 'Auth\AuthController@register');
});

	Route::group(['middleware'=>['web', 'auth']], function () {
	// user dashboard
	Route::get('/dashboard', 'DashboardController@index');
	
	// loads
	Route::get('loads', 'LoadController@index');
	Route::get('loads/new', 'LoadController@create');
	Route::post('loads', 'LoadController@store');
	Route::get('loads/{id}/edit', 'LoadController@edit');
	
	//companies
	Route::get('companies', 'CompanyController@index');
	Route::get('companies/new', 'CompanyController@create');
	Route::post('companies', 'CompanyController@store');
	Route::get('companies/{id}/edit', 'CompanyController@edit');
	Route::put('companies/{id}', 'CompanyController@update');
	Route::get('autocompleteCompany', 'CompanyController@autocomplete');
	
	//companyRatings
	Route::get('companyRatings', 'companyRatingController@index');
	Route::get('companyRatings/new', 'companyRatingController@create');
	Route::post('companyRatings', 'companyRatingController@store');
	Route::get('companyRatings/{id}/edit', 'companyRatingController@edit');
	Route::put('companyRatings/{id}', 'companyRatingController@update');
	
	//brokers
	Route::get('brokers/{id}', 'BrokerController@show');
	Route::get('/brokers/{id}/new', 'BrokerController@create');
	Route::post('brokers', 'BrokerController@store');
	Route::get('brokers/{id}/edit', 'BrokerController@edit');
	Route::put('brokers/{id}', 'BrokerController@update');
	Route::get('autocompleteBroker', 'BrokerController@autocomplete');
	
	//admin
	Route::get('admin', 'AdminController@index');
	Route::get('admin/types', 'AdminController@types');
	
	//trucks
	Route::get('trucks', 'TruckController@index');
	Route::get('trucks/new', 'TruckController@create');
	Route::post('trucks', 'TruckController@store');
	Route::get('trucks/{id}/edit', 'TruckController@edit');
	Route::put('trucks/{id}', 'TruckController@update');
	
	//drivers
	Route::get('drivers', 'DriverController@index');
	Route::get('drivers/new', 'DriverController@create');
	Route::post('drivers', 'DriverController@store');
	Route::get('drivers/{id}/edit', 'DriverController@edit');
	Route::put('drivers/{id}', 'DriverController@update');
	
	//driver types
	Route::get('/driverTypes', 'DriverTypeController@index');
	Route::get('driverTypes/new', 'DriverTypeController@create');
	Route::post('driverTypes', 'DriverTypeController@store');
	Route::get('driverTypes/{id}/edit', 'DriverTypeController@edit');
	Route::put('driverTypes/{id}', 'DriverTypeController@update');
});
