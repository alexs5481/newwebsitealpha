@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">New Truck</div>

                <div class="panel-body">
				
					{{Form::open(array('url'=>'brokers', 'method'=>'post', 'class'=>'form-horizontal'))}}
					{{Form::hidden('companyId', $companyId)}}
					<div class='form-group'>
						<label class='col-sm-2 control-label'>First Name: </label>
						<div class='col-sm-10'>
							{{Form::text('fname', '', array("class"=>"form-control"))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Last Name:</label>
						<div class='col-sm-10'>
							{{Form::text('lname', '', array("class"=>"form-control"))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Phone Number:</label>
						<div class='col-sm-10'>
							{{Form::text('phoneNumber', '', array("class"=>"form-control"))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Email:</label>
						<div class='col-sm-10'>
							{{Form::text('email', '', array("class"=>"form-control"))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'></label>
						<div class='col-sm-10'>
							{{Form::submit('Submit', array("class"=>"btn btn-primary"))}}
							<a class="btn btn-default" href="/companies">Cancel</a>
							{{Form::close()}}
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
