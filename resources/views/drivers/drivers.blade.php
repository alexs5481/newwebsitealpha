@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading ">Drivers Page
								<a href="drivers/new" class="btn btn-primary pull-right heading-panel-button " id="">New Drivers</a>
				</div>
                <div class="panel-body">
                    @forelse ($drivers as $driver)
					
					<div class="clickable showable" data-object="drivers" data-id="{{$driver->id}}">
                         	<div class="row">
								<div class="col-md-10">
									<div class="col-md-5">
										<strong>Name:</strong>  {{$driver->fname . " " . $driver->lname}}
										<br>
										<strong>Driver Type:</strong>  {{$driver->types->name}}
										<br>
										<strong>Phone Number:</strong>  {{$driver->phoneNumber}}
										<br>
										<strong>Date Of Birth:</strong>  {{$driver->DOB}}
										<br>
									</div>
                            	</div>
                        	</div>
							<hr/>
                    </div>
					@empty
						Table with Drivers goes here
					@endforelse
					
					<br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
