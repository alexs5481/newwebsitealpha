@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Brokers Ratings Show Company 
					<a href="/brokers/<?php echo $companyId;?>/new" class="btn btn-primary heading-panel-button pull-right">Add New Broker</a>
				</div>
				
                <div class="panel-body">
					@forelse ($brokers as $broker)
					
					<div class="clickable showable" data-object="brokers" data-id="{{$broker->id}}">
                         	<div class="row">
								<div class="col-md-10">
									<div class="col-md-5">
										<strong>Type:</strong>  {{$broker->fname}}
									</div>
                            	</div>
                        	</div>
							<hr/>
                    </div>
					@empty
						Table with Brokers goes here
					@endforelse
					
					<br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
