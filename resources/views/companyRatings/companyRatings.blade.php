@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Company Ratings
					<a href="companies/new" class="btn btn-primary heading-panel-button pull-right">New Company Rating</a>
				</div>
                <div class="panel-body">
				
                    @forelse ($companyRatings as $companyRating)
					<div>
                         	<div class="row form-horizontal">
								<div class="col-md-10 form-group">
									<div class="col-xs-2 pull-left">
									</div>
									<div class="clickable showable col-xs-4" data-object="companies" data-id="{{$company->id}}" class="col-xs-4">
										<strong>Company:</strong> {{$companyRating->companies->name}}
										<br>
										<strong>Rating Type:</strong>  {{$companyRating->ratingType}}
										<br>
										<strong>Date:</strong>  {{$companyRating->date}}
										<br>
									</div>
									<div class='col-xs-4 pull-right'>
										<a href="brokers/<?php echo $company->id;?>" class="btn btn-primary pull-right">Brokers</a>
									</div>
                            	</div>
                        	</div>
							<hr/>
                    </div>
					@empty
						Table with Company Ratings goes here
					@endforelse
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){

		$('input:text').bind({
			
		});
		$( "#company" ).autocomplete({
		source: '{{URL('autocompleteCompany')}}'
		});
	});
</script>

@endsection
