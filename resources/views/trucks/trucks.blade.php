@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Trucks Page
					<a href="trucks/new" class="btn btn-primary heading-panel-button pull-right">New Truck</a>
				</div>
                <div class="panel-body">
                    @forelse ($trucks as $truck)
					
					<div class="clickable showable" data-object="trucks" data-id="{{$truck->id}}">
                         	<div class="row">
								<div class="col-md-10">
									<div class="col-md-5">
										<strong>Truck# </strong>  {{$truck->number}} 
										<br>
										<strong>Driver:</strong>  {{$truck->drivers->fname . "" . $truck->drivers->lname }} 
										<br>
										<strong>Year:</strong>  {{$truck->years->year}} 
										<br>
										<strong>Vin:</strong>  {{$truck->vin}} 
										<br>
										<strong>Make:</strong>  {{$truck->makes->name}} 
										<br>
										<strong>Model:</strong>  {{$truck->models->name}}
										<br>
									</div>
                            	</div>
                        	</div>
							<hr/>
                    </div>
					@empty
						Table with Trucks goes here
					@endforelse
					
					<br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
