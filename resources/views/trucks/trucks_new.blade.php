@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">New Truck</div>

                <div class="panel-body">
				
					{{Form::open(array('url'=>'trucks', 'method'=>'post', 'class'=>'form-horizontal'))}}
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Truck Number: </label>
						<div class='col-sm-10'>
							{{Form::text('number', '', array("class"=>"form-control"))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Driver:</label>
						<div class='col-sm-10'>
							{{Form::select('driver', $driverArray, '1', array('class' => 'form-control'))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Year:</label>
						<div class='col-sm-10'>
							{{Form::select('year', $yearArray, '1', array('class' => 'form-control'))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>VIN:</label>
						<div class='col-sm-10'>
							{{Form::text('vin', '', array("class"=>"form-control"))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Make:</label>
						<div class='col-sm-10'>
							{{Form::select('make', $makeArray, '1', array('class' => 'form-control'))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Model:</label>
						<div class='col-sm-10'>
							{{Form::select('model', $modelArray, '1', array('class' => 'form-control'))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'></label>
						<div class='col-sm-10'>
							{{Form::submit('Submit', array("class"=>"btn btn-primary"))}}
							<a class="btn btn-default" href="/trucks">Cancel</a>
							{{Form::close()}}
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
