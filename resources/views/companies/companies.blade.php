@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Companies
					<a href="companies/new" class="btn btn-primary heading-panel-button pull-right">New Company</a>
				</div>
                <div class="panel-body">
				
					{{Form::open(array('url'=>'','method'=>'POST', 'class'=>'form-horizontal'))}}
					<div class="form-group">
						<label class='col-sm-2 control-label'>Search:</label>
						<div class="col-sm-7">
						<input class="form-control" type="text" id="company" name='company' disabled>
						</div>
						<div class="col-md-2">
							{{Form::submit('Submit', array("class"=>"btn btn-primary", 'disabled'))}}
						</div>
					</div>
					{{Form::close()}}
					
					<table>
						<tr>
							<td>Name</td>
							<td>State</td>
							<td>MC</td>
							<td>DOT</td>
							<td>Rating</td>
							<td>Comments</td>
							<td>Brokers</td>
						</tr>

						@forelse ($companies as $company)
						<tr>
							<td>{{$company->name}}</td>
							<td>State</td>
							<td>{{$company->mc}}</td>
							<td>{{$company->dot}}</td>
							<td>{{$company->ratings->name}}</td>
							<td>{{$company->comments}}</td>
							<td>Brokers</td>
						</tr>
						@empty
							Table with Companies goes here
						@endforelse
					</table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){

		$('input:text').bind({
			
		});
		$( "#company" ).autocomplete({
		source: '{{URL('autocompleteCompany')}}'
		});
	});
</script>

@endsection
