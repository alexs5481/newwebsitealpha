@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">New Company</div>

                <div class="panel-body">
				
					{{Form::open(array('url'=>'companies', 'method'=>'post', 'class'=>'form-horizontal'))}}
						{{Form::hidden('newCompany', 0)}}
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Name:</label>
						<div class='col-sm-10'>
							{{Form::text('name', '', array("class"=>"form-control"))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>City:</label>
						<div class='col-sm-10'>
							{{Form::text('city', '', array("class"=>"form-control"))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>State:</label>
						<div class='col-sm-10'>
							{{Form::select('state', $stateArray, '1', array('class' => 'form-control'))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>MC: </label>
						<div class='col-sm-10'>
							{{Form::text('mc', '', array("class"=>"form-control"))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>DOT:</label>
						<div class='col-sm-10'>
							{{Form::text('dot', '', array("class"=>"form-control"))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Company Type:</label>
						<div class='col-sm-10'>
							{{Form::select('type', $companyTypesArray, '1', array('class' => 'form-control'))}}
						</div>
					</div>
                    
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Company Rating:</label>
						<div class='col-sm-10'>
							{{Form::select('rating', $ratingTypeArray, '1', array('class' => 'form-control'))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Comments:</label>
						<div class='col-sm-10'>
							{{Form::text('comments', '', array("class"=>"form-control"))}}
						</div>
					</div>
                    
					<div class='form-group'>
						<div class='col-sm-2 control-label'></div>
						<div class='col-sm-10'>
							{{Form::submit('Submit', array("class"=>"btn btn-primary"))}}
							<a class="btn btn-default" href="/companies">Cancel</a>
							{{Form::close()}}
						</div>
					</div>
					
					
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
