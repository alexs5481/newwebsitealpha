@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Company</div>

                <div class="panel-body">
				
					{{Form::open(array('url'=>'companies/' . $company->id, 'method'=>'put', 'class'=>'form-horizontal'))}}
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Name:</label>
						<div class='col-sm-10'>
							{{Form::text('name', $company->name, array("class"=>"form-control"))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>City:</label>
						<div class='col-sm-10'>
							{{Form::text('city', $company->city, array("class"=>"form-control"))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>State:</label>
						<div class='col-sm-10'>
							{{Form::select('state', $stateArray, $company->state, array('class' => 'form-control'))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>MC:</label>
						<div class='col-sm-10'>
							{{Form::text('mc', $company->mc, array("class"=>"form-control"))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>DOT:</label>
						<div class='col-sm-10'>
							{{Form::text('dot', $company->dot, array("class"=>"form-control"))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Company Type:</label>
						<div class='col-sm-10'>
							{{Form::select('type', $companyTypesArray, $company->companyType, array('class' => 'form-control'))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Rating:</label>
						<div class='col-sm-10'>
							{{Form::select('rating', $ratingTypeArray, $company->rating, array('class' => 'form-control'))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Comments:</label>
						<div class='col-sm-10'>
							{{Form::text('comments', $company->comments, array("class"=>"form-control"))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'></label>
						<div class='col-sm-10'>
							{{Form::submit('Submit', array("class"=>"btn btn-primary"))}}
							<a class="btn btn-default" href="/companies">Cancel</a>
							{{Form::close()}}
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
