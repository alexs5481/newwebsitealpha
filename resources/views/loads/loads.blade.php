@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading ">Loads
					<a href="loads/new" class="btn btn-primary pull-right heading-panel-button">Add Load</a>
				</div>
                <div class="panel-body">
				
                    {{Form::open(array('url'=>'','method'=>'POST', 'class'=>'form-horizontal'))}}
					<div class="form-group">
						<label class='col-sm-2 control-label'>Search:</label>
						<div class="col-sm-7">
						<input class="form-control" type="text" id="company" name='company' disabled>
						</div>
						<div class="col-md-2">
							{{Form::submit('Submit', array("class"=>"btn btn-primary", 'disabled'))}}
						</div>
					</div>
					{{Form::close()}}

					<table>
						<tr>
							<td>PRO#</td>
							<td>Company</td>
							<td>Truck#</td>
							<td>Pay</td>
							<td>Broker's Name</td>
							<td>Posted Date</td>
							<td>Comments</td>
						</tr>

						@forelse ($loads as $load)
						<tr>
							<td>{{$load->proNumber}}</td>
							<td>{{$load->companies->name}}</td>
							<td>{{$load->trucks->number}}</td>
							<td>{{$load->loadAmount}}</td>
							<td>{{$load->broker}}</td>
							<td>Some Date</td>
							<td>{{$load->comments}}</td>
						</tr>
						@empty
							Table With Loads Goes Here
						@endforelse
					</table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
