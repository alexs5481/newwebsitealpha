@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">New Loads</div>

                <div class="panel-body">
				
					{{Form::open(array('url'=>'loads' . $load->id, 'method'=>'put', 'class'=>'form-horizontal'))}}
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Company: </label>
						<div class='col-sm-10'>
							{{Form::text('company', $load->companies->name, array("class"=>"form-control", "id"=>"company", 'onblur'=>"enableTextBox(this)"))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Truck Number:</label>
						<div class='col-sm-10'>
							{{Form::select('truckNumber', $truckArray, $load->truck, array('class' => 'form-control'))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Amount: $</label>
						<div class='col-sm-10'>
							{{Form::text('amount', $load->loadAmount, array("class"=>"form-control"))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Driver Pay: $</label>
						<div class='col-sm-10'>
							{{Form::text('driverPay', $load->driverPay, array("class"=>"form-control"))}}
						</div>
					</div>
						
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Broker:</label>
						<div class='col-sm-10'>
							<?php if($load->broker != 0): ?>
							{{Form::text('broker', $load->broker, array("class"=>"form-control", 'id'=>'broker'))}}
							<?php else:?>							
							{{Form::text('broker', '', array("class"=>"form-control", 'id'=>'broker'))}}
							<?php endif;?>
							
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Broker Rating:</label>
						<div class='col-sm-10'>
							{{Form::select('brokerRating', $ratingTypeArray, '', array('class' => 'form-control'))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Load Type:</label>
						<div class='col-sm-10'>
							{{Form::select('loadType', $loadTypeArray, $load->loadType, array('class' => 'form-control'))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Comments:</label>
						<div class='col-sm-10'>
							{{Form::text('comments', $load->comments, array("class"=>"form-control"))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'></label>
						<div class='col-sm-10'>
							{{Form::submit('Submit', array("class"=>"btn btn-primary"))}}
							<a class="btn btn-default" href="/loads">Cancel</a>
							{{Form::close()}}
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('input:text').bind({});
		$('#test').html('helloWorld');
		$( "#company" ).autocomplete({
			source: '{{URL('autocompleteCompany')}}'
		});
		$('#broker').autocomplete({
			source: function(request, response){
				$.ajax({
					url: 'http://localhost:8000/autocompleteBroker/',
					dataType: 'json',
					data: {
						term : request.term,
						companyName : $('#company').val()
					},
					success: function(data) {
						response(data);
					}
				});
			},
			minLength: 1,
			delay: 300
		});
	});
	function enableTextBox(id){
		if(!id.value){	
			document.getElementById('broker').setAttribute("disabled", "disabled");
		}else{
			document.getElementById('broker').removeAttribute("disabled");
		}
	}
	
</script>
@endsection
