@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Admin Stuff</div>

                <div class="panel-body">
                    Admin stuff
					<br>
					<br>

                    <div class="row">
                       <div class="col-md-10 col-md-offset-1">
                           <div class="panel panel-default">
                               <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-11">Trucks</div>
                                    <div class="col-md-1">
                                        <span class="glyphicon glyphicon-plus admin-add" data-toggle= "modal" data-target= "#trucks_modal"></span>
                                    </div>
                                </div>
                            </div>
                                <div class="panel-collapse">
                                    <table class="table table-responsive table-condensed">
                                        <thead>
                                            <th class="name-column">Truck#</th>
                                            <th></th>
                                        </thead>
                                        @foreach($trucks as $truck)
                                        <tr>
                                            <td>{{$truck->id}}</td>
                                            <td>{{Form::open(array('url' => '/trucks/' . $truck -> id, 'method' => 'delete'))}}<span class="glyphicon glyphicon-remove admin-delete"></span>{{Form::close()}}
                                        </tr> 
                                        @endforeach
                                    </table>
                                </div>
                           </div>
                       </div>
                    </div>

                    <div class="row">
                       <div class="col-md-10 col-md-offset-1">
                           <div class="panel panel-default">
                               <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-11">Trucks</div>
                                    <div class="col-md-1">
                                        <span class="glyphicon glyphicon-plus admin-add" data-toggle= "modal" data-target= "#drivers_modal"></span>
                                    </div>
                                </div>
                            </div>
                                <div class="panel-collapse">
                                    <table class="table table-responsive table-condensed">
                                        <thead>
                                            <th class="name-column">Drivers</th>
                                            <th></th>
                                        </thead>
                                        @foreach($drivers as $driver)
                                        <tr>
                                            <td>{{$driver->id}}</td>
                                            <td>{{Form::open(array('url' => '/drivers/' . $driver -> id, 'method' => 'delete'))}}<span class="glyphicon glyphicon-remove admin-delete"></span>{{Form::close()}}
                                        </tr> 
                                        @endforeach
                                    </table>
                                </div>
                           </div>
                       </div>
                    </div>
                    <br>
                    <br>

                    <br>
                    <br>

					<a href="trucks" class="btn btn-primary">Trucks</a>
					<a href="drivers" class="btn btn-primary">Drivers</a>
					<a href="admin/types" class="btn btn-primary">Types</a>
					<a href="companyRatings" class="btn btn-primary">Company Ratings</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="trucks_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">New Truck</h4>
      </div>
      <div class="modal-body">
        {{Form::open(array('url'=>'trucks', 'method'=>'post'))}}
        <label>Truck#: </label>
        {{Form::text('truckNumber', '', array("class"=>"form-control"))}}  
      </div>
      <div class="modal-footer">
    {{Form::submit('Submit', array("class"=>"btn btn-primary"))}}
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    {{Form::close()}} 
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="drivers_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">New Driver</h4>
      </div>
      <div class="modal-body">
        {{Form::open(array('url'=>'driver', 'method'=>'post'))}}
        <label>Driver Name: </label>
        {{Form::text('driverName', '', array("class"=>"form-control"))}}  
      </div>
      <div class="modal-footer">
    {{Form::submit('Submit', array("class"=>"btn btn-primary"))}}
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    {{Form::close()}} 
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection
