@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Types Stuff</div>

                <div class="panel-body">
                    Types stuff
					<br>
					<br>
					<a href="/driverTypes" class="btn btn-primary">Driver Types</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
