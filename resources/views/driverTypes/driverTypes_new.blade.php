@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">New Driver Types</div>

                <div class="panel-body">
				
					{{Form::open(array('url'=>'driverTypes', 'method'=>'post', 'class'=>'form-horizontal'))}}
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'>Driver Type Name: </label>
						<div class='col-sm-10'>
							{{Form::text('name', '', array("class"=>"form-control"))}}
						</div>
					</div>
					
					<div class='form-group'>
						<label class='col-sm-2 control-label'></label>
						<div class='col-sm-10'>
							{{Form::submit('Submit', array("class"=>"btn btn-primary"))}}
							<a class="btn btn-default" href="/driverTypes">Cancel</a>
							{{Form::close()}}
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
