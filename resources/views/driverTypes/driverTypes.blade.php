@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Driver Types Page
					<a href="driverTypes/new" class="btn btn-primary heading-panel-button pull-right">New Driver Types</a>
				</div>
				
                <div class="panel-body">
					@forelse ($driverTypes as $driverTypes)
					
					<div class="clickable showable" data-object="driverTypes" data-id="{{$driverTypes->id}}">
                         	<div class="row">
								<div class="col-md-10">
									<div class="col-md-5">
										<strong>Type:</strong>  {{$driverTypes->name}}
									</div>
                            	</div>
                        	</div>
							<hr/>
                    </div>
					@empty
						Table with Driver Types goes here
					@endforelse
					
					<br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
